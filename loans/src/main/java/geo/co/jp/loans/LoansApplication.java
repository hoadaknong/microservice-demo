package geo.co.jp.loans;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditAwareImpl")
@OpenAPIDefinition(
        info = @Info(
                title = "Loan microservice REST API Documentation",
                description = "Loan microservice REST API Documentation",
                version= "v1",
                contact = @Contact(
                        name = "Pham Dinh Quoc Hoa",
                        email = "pham.dinh.quoc.hoa@geonet.co.jp",
                        url = "https://geo-ssv.vn/company/vnm/"
                ),
                license = @License(
                        name="Apache 2.0",
                        url="https://geo-ssv.vn/company/vnm/"
                )
        ),
        externalDocs = @ExternalDocumentation(
                description = "Loan microservice REST API Documentation",
                url="https://geo-ssv.vn/company/vnm/"
        )
)
public class LoansApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoansApplication.class, args);
    }

}
