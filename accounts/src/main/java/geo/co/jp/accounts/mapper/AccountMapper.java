package geo.co.jp.accounts.mapper;

import geo.co.jp.accounts.dto.AccountDto;
import geo.co.jp.accounts.entity.Account;

public class AccountMapper {
    private AccountMapper() {
    }

    public static AccountDto mapToAccountDto(Account account, AccountDto dto) {
        dto.setAccountNumber(account.getId());
        dto.setAccountType(account.getAccountType());
        dto.setBranchAddress(account.getBranchAddress());

        return dto;
    }

    public static Account mapToAccount(AccountDto dto, Account account) {
        account.setAccountType(dto.getAccountType());
        account.setBranchAddress(dto.getBranchAddress());

        return account;
    }
}
