package geo.co.jp.accounts.service.impl;

import geo.co.jp.accounts.constant.AccountConstant;
import geo.co.jp.accounts.dto.AccountDto;
import geo.co.jp.accounts.dto.CustomerDto;
import geo.co.jp.accounts.entity.Account;
import geo.co.jp.accounts.entity.Customer;
import geo.co.jp.accounts.exception.CustomerAlreadyExistException;
import geo.co.jp.accounts.exception.ResourceNotFoundException;
import geo.co.jp.accounts.mapper.AccountMapper;
import geo.co.jp.accounts.mapper.CustomerMapper;
import geo.co.jp.accounts.repository.AccountRepository;
import geo.co.jp.accounts.repository.CustomerRepository;
import geo.co.jp.accounts.service.IAccountService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AccountServiceImpl implements IAccountService {

    private AccountRepository accountRepository;

    private CustomerRepository customerRepository;

    /**
     * @param customerDto - CustomerDto Object
     */
    @Override
    public void createAccount(CustomerDto customerDto) {
        Customer customer = CustomerMapper.mapToCustomer(customerDto, new Customer());
        Optional<Customer> customerByPhoneNumber = customerRepository
                .findCustomerByPhoneNumber(customerDto.getPhoneNumber());
        if (customerByPhoneNumber.isPresent()) {
            throw new CustomerAlreadyExistException(
                    String.format("Phone number %s have already registered", customerDto.getPhoneNumber())
            );
        }

        Customer customerSaved = customerRepository.save(customer);
        accountRepository.save(createNewAccount(customerSaved));
    }

    /**
     * @param phoneNumber - String
     * @return - CustomerDto
     */
    @Override
    public CustomerDto getAccountByPhoneNumber(String phoneNumber) {
        Customer customer = customerRepository
                .findCustomerByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new ResourceNotFoundException("Customer", "phoneNumber", phoneNumber));

        Account account = accountRepository
                .findAccountByCustomerId(customer.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Account", "customerId", customer.getId().toString()));

        CustomerDto dto = CustomerMapper.mapToCustomerDto(customer, new CustomerDto());
        dto.setAccountDto(AccountMapper.mapToAccountDto(account, new AccountDto()));

        return dto;
    }

    /**
     * @param id  - Long
     * @param dto - CustomerDto Object
     * @return - boolean
     */
    @Override
    public boolean updateAccount(Long id, CustomerDto dto) {
        if (dto.getAccountDto() == null) {
            return false;
        }

        Customer customer = customerRepository
                .findById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException("Customer", "id", id.toString())
                );

        Account account = accountRepository
                .findById(dto.getAccountDto().getAccountNumber())
                .orElseThrow(
                        () -> new ResourceNotFoundException("Account", "id", dto.getAccountDto().getAccountNumber().toString())
                );

        customerRepository.save(CustomerMapper.mapToCustomer(dto, customer));
        accountRepository.save(AccountMapper.mapToAccount(dto.getAccountDto(), account));

        return true;
    }

    /**
     * @param phoneNumber - String
     * @return - Boolean
     */
    @Override
    public boolean deleteAccount(String phoneNumber) {
        Customer customer = customerRepository
                .findCustomerByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new ResourceNotFoundException("Customer", "phoneNumber", phoneNumber));
        Account account = accountRepository
                .findAccountByCustomerId(customer.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Account", "customerId", customer.getId().toString()));

        customerRepository.delete(customer);
        accountRepository.delete(account);

        return true;
    }

    /**
     * @param customer - Customer Object
     * @return account
     */
    private Account createNewAccount(Customer customer) {
        Account account = new Account();
        account.setCustomerId(customer.getId());
        account.setAccountType(AccountConstant.SAVING);
        account.setBranchAddress(AccountConstant.ADDRESS);

        return account;
    }
}
