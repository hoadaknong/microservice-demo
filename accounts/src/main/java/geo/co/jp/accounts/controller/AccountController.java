package geo.co.jp.accounts.controller;

import geo.co.jp.accounts.constant.AccountConstant;
import geo.co.jp.accounts.dto.CustomerDto;
import geo.co.jp.accounts.dto.ErrorResponseDto;
import geo.co.jp.accounts.dto.ResponseDto;
import geo.co.jp.accounts.service.IAccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@Tag(
        name = "CRUD REST APIs for Account in Bank",
        description = "CRUD REST APIs in Bank to CREATE, UPDATE, FETCH AND DELETE account details"
)
@RestController
@RequestMapping(path = "api/v1/account")
@Validated
@Slf4j
public class AccountController {

    @Value("${build.version}")
    private String buildVersion;

    private final IAccountService iAccountService;

    public AccountController(IAccountService iAccountService){
        this.iAccountService = iAccountService;
    }

    @Operation(
            summary = "Create Account REST API",
            description = "REST API to create a new Customer and Account in Bank"
    )
    @ApiResponse(
            responseCode = "201",
            description = "HTTP status CREATED"
    )
    @PostMapping
    public ResponseEntity<ResponseDto> create(
            @RequestBody
            @Valid
            CustomerDto customerDto) {
        iAccountService.createAccount(customerDto);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new ResponseDto(
                        AccountConstant.STATUS_201,
                        AccountConstant.MESSAGE_201
                ));
    }

    @Operation(
            summary = "Get Account REST API",
            description = "REST API to get Customer and Account details in Bank"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP status OK"
    )
    @GetMapping("{phoneNumber}")
    public ResponseEntity<CustomerDto> getByPhoneNumber(
            @PathVariable
            @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile number must be 10 digits")
            String phoneNumber
    ) {
        CustomerDto customerDto = iAccountService.getAccountByPhoneNumber(phoneNumber);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(
                        customerDto
                );
    }

    @GetMapping("build-version")
    public ResponseEntity<String> getBuildVersion(
    ) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(
                        buildVersion
                );
    }

    @Operation(
            summary = "Update Account REST API",
            description = "REST API to update Customer and Account details in Bank"
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "HTTP status OK"
            ),
            @ApiResponse(
                    responseCode = "417",
                    description = "HTTP status EXPECTATION_FAILED"
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "HTTP status INTERNAL SERVER ERROR",
                    content = @Content(
                            schema = @Schema(implementation = ErrorResponseDto.class)
                    )
            )
    })
    @PutMapping("{id}")
    public ResponseEntity<ResponseDto> updateAccount(
            @PathVariable
            Long id,
            @Valid
            @RequestBody
            CustomerDto dto) {
        boolean isUpdated = iAccountService.updateAccount(id, dto);

        return isUpdated
                ? ResponseEntity
                .status(HttpStatus.OK)
                .body(
                        new ResponseDto(
                                AccountConstant.STATUS_200,
                                AccountConstant.MESSAGE_200
                        )
                )
                : ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .body(
                        new ResponseDto(
                                AccountConstant.STATUS_417,
                                AccountConstant.MESSAGE_417
                        )
                );
    }

    @Operation(
            summary = "Delete Account REST API",
            description = "REST API to delete Customer and Account details in Bank"
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "HTTP status OK"
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "HTTP status INTERNAL SERVER ERROR",
                    content = @Content(
                            schema = @Schema(implementation = ErrorResponseDto.class)
                    )
            )
    })
    @DeleteMapping("{phoneNumber}")
    public ResponseEntity<ResponseDto> deleteAccount(
            @PathVariable
            @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile number must be 10 digits")
            String phoneNumber) {
        boolean isDeleted = iAccountService.deleteAccount(phoneNumber);

        return isDeleted
                ? ResponseEntity
                .status(HttpStatus.OK)
                .body(
                        new ResponseDto(
                                AccountConstant.STATUS_200,
                                AccountConstant.MESSAGE_200
                        )
                )
                : ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(
                        new ResponseDto(
                                AccountConstant.STATUS_500,
                                AccountConstant.MESSAGE_500
                        )
                );
    }
}
