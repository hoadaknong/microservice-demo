package geo.co.jp.accounts.service;

import geo.co.jp.accounts.dto.CustomerDto;

public interface IAccountService {

    /**
     * @param customerDto - CustomerDto Object
     */
    void createAccount(CustomerDto customerDto);

    /**
     * @param phoneNumber - String
     * @return - CustomerDto
     */
    CustomerDto getAccountByPhoneNumber(String phoneNumber);

    /**
     * @param id  - Long
     * @param dto - CustomerDto Object
     * @return - boolean
     */
    boolean updateAccount(Long id, CustomerDto dto);

    /**
     *
     * @param phoneNumber - String
     * @return - Boolean
     */
    boolean deleteAccount(String phoneNumber);
}
