package geo.co.jp.accounts.mapper;

import geo.co.jp.accounts.dto.CustomerDto;
import geo.co.jp.accounts.entity.Customer;

public class CustomerMapper {
    private CustomerMapper() {
    }

    public static CustomerDto mapToCustomerDto(Customer customer, CustomerDto dto) {
        dto.setEmail(customer.getEmail());
        dto.setPhoneNumber(customer.getPhoneNumber());
        dto.setName(customer.getName());

        return dto;
    }

    public static Customer mapToCustomer(CustomerDto dto, Customer customer) {
        customer.setName(dto.getName());
        customer.setPhoneNumber(dto.getPhoneNumber());
        customer.setEmail(dto.getEmail());

        return customer;
    }
}
